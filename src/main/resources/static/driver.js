import React from 'react';
import ReactDOM from 'react-dom';
import ApplicationController from './controllers/ApplicationController';

ReactDOM.render(<ApplicationController />, document.getElementById('root'));
