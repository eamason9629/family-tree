import React, { Component } from 'react';

class EditPersonView extends Component {
    constructor() {
        super();

        this.state = {firstName: '', middleName: '', lastName: '', nickname: '', maidenName: '', placeOfBirth: '', placeOfDeath: ''};

        this.doSave = this.doSave.bind(this);
        this.onChangeField = this.onChangeField.bind(this);
    }

    componentDidMount() {
        if(this.props.identifier) {
            let url = '/people/' + this.props.identifier;
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    this.setState(Object.assign(this.state, data));
                });
        }
    }

    doSave(event) {
        event.preventDefault();
        event.stopPropagation();
        let method = this.state.identifier ? 'put' : 'post';
        let url = '/people/' + (this.state.identifier ? this.state.identifier : '');
        fetch(url, {
            method: method,
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(this.state)
        })
            .then(response => response.json())
            .then(data => {
                this.setState(Object.assign({}, data));
            });
    }

    onChangeField(event) {
        let result = Object.assign({}, this.state);
        result[event.target.name] = event.target.value;
        this.setState(result);
    }

    renderField(field) {
        return (
            <label htmlFor={field.name} key={field.name} className='input-group'>
                <span className='input-group-prepend'>
                    <span className='input-group-text'>{field.prettyName}</span>
                </span>
                <input type='text' placeholder={field.prettyName} name={field.name} value={this.state[field.name] || ''} onChange={this.onChangeField}></input>
            </label>
        );
    }

    render() {
        let fields = [
            {name: 'firstName', prettyName: 'First Name'},
            {name: 'middleName', prettyName: 'Middle Name'},
            {name: 'lastName', prettyName: 'Last Name'},
            {name: 'nickname', prettyName: 'Nickname'},
            {name: 'maidenName', prettyName: 'Maiden Name'},
            {name: 'placeOfBirth', prettyName: 'Birth Place'},
            {name: 'placeOfDeath', prettyName: 'Death Place'}
        ];
        return (
            <form onSubmit={this.doSave}>
                <fieldset>
                    <legend>Edit Person</legend>
                    {fields.map(field => {
                        return this.renderField(field);
                    })}
                    <button type='submit' onClick={this.doSave}>Save!</button>
                </fieldset>
            </form>
        );
    }
}

export default EditPersonView;
