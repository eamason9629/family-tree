import React, { Component } from 'react';

class NavigationView extends Component {
    render() {
        return (
            <header>
                <ul className='nav nav-tabs'>
                    <li className='nav-item'>
                        <a className={`nav-link${this.props.selected == 'home' ? ' active' : ''}`} href='#' onClick={() => { this.props.linkClicked('home') }}>
                            Home
                        </a>
                    </li>
                    <li className='nav-item'>
                        <a className={`nav-link${this.props.selected == 'searchPeople' ? ' active' : ''}`} href='#' onClick={() => { this.props.linkClicked('searchPeople') }}>
                            People
                        </a>
                    </li>
                    <li className='nav-item'>
                        <a className={`nav-link${this.props.selected == 'tree' ? ' active' : ''}`} href='#' onClick={() => { this.props.linkClicked('tree') }}>
                            Family Tree
                        </a>
                    </li>
                </ul>
            </header>
        );
    }
}

export default NavigationView;