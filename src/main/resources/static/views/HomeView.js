import React, { Component } from 'react';

class HomeView extends Component {
    render() {
        return (
            <div>
                Welcome to the Family Tree tool! This tool will allow you to manage your family tree, manage relationships between people, and produce a pretty family tree!
            </div>
        );
    }
}

export default HomeView;