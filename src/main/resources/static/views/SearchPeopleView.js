import React, { Component } from 'react';

class SearchPeopleView extends Component {
    constructor() {
        super();

        this.state = {
            results: [],
            searchParams: {firstName: '', middleName: '', lastName: '', nickname: '', maidenName: '', placeOfBirth: '', placeOfDeath: ''}
        };

        this.doCreate = this.doCreate.bind(this);
        this.doSearch = this.doSearch.bind(this);
        this.onChangeSearchField = this.onChangeSearchField.bind(this);
        this.onEditPerson = this.onEditPerson.bind(this);
    }

    doSearch(event) {
        event.preventDefault();
        event.stopPropagation();
        let query = ''
        if(Object.keys(this.state.searchParams).length > 0) {
            query = '?';
            Object.keys(this.state.searchParams).forEach(key => {
                if(query.length > 1) {
                    query += '&';
                }
                let value = this.state.searchParams[key].length > 0 ? `%${this.state.searchParams[key]}%` : ''
                query += key + '=' + encodeURIComponent(value);
            });
        }
        let url = (query == '?firstName=&middleName=&lastName=&nickname=&maidenName=&placeOfBirth=&placeOfDeath=' ? '/people' : ('/people/search/people' + query));
        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({results: data._embedded.people});
            });
    }

    onChangeSearchField(event) {
        let result = Object.assign({}, this.state.searchParams);
        result[event.target.name] = event.target.value;
        this.setState({searchParams: result});
        this.doSearch(event);
    }

    renderSearchField(field) {
        return (
            <label htmlFor={field.name} key={field.name} className='input-group'>
                <span className='input-group-prepend'>
                    <span className='input-group-text'>{field.prettyName}</span>
                </span>
                <input type='text' placeholder={field.prettyName} name={field.name} value={this.state[field.name]} onChange={this.onChangeSearchField}></input>
            </label>
        );
    }

    renderSearchForm() {
        let fields = [
            {name: 'firstName', prettyName: 'First Name'},
            {name: 'middleName', prettyName: 'Middle Name'},
            {name: 'lastName', prettyName: 'Last Name'},
            {name: 'nickname', prettyName: 'Nickname'},
            {name: 'maidenName', prettyName: 'Maiden Name'},
            {name: 'placeOfBirth', prettyName: 'Birth Place'},
            {name: 'placeOfDeath', prettyName: 'Death Place'}
        ];
        return (
            <form onSubmit={this.doSearch}>
                <fieldset>
                    <legend>Find and edit people</legend>
                    {fields.map(field => {
                        return this.renderSearchField(field);
                    })}
                    <button onClick={this.doCreate} style={{marginLeft: '5px'}}>Create Person</button>
                </fieldset>
            </form>
        );
    }

    doCreate(event) {
        event.preventDefault();
        event.stopPropagation();
        this.props.onEditPerson();
    }

    onEditPerson(event) {
        this.props.onEditPerson(event.target.dataset.identifier);
    }

    renderPersonRow(person) {
        return (
            <tr key={person.identifier}>
                <td className='first-name'>{person.firstName}</td>
                <td className='middle-name'>{person.middleName}</td>
                <td className='last-name'>{person.lastName}</td>
                <td className='birth-place'>{person.placeOfBirth}</td>
                <td className='death-place'>{person.placeOfDeath}</td>
                <td className='edit-person'><a href='#' onClick={this.onEditPerson} data-identifier={person.identifier}>Edit</a></td>
            </tr>
        );
    }

    renderResults() {
        return (
            <table className='table table-striped' style={{marginTop:'10px'}}>
                <thead>
                    <tr>
                        <th scope='col first-name'>First Name</th>
                        <th scope='col middle-name'>Middle Name</th>
                        <th scope='col last-name'>Last Name</th>
                        <th scope='col birth-place'>Birth Place</th>
                        <th scope='col death-place'>Death Place</th>
                        <th scope='col edit-person'></th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.results.map(person => {
                        return this.renderPersonRow(person);
                    })}
                </tbody>
            </table>
        );
    }

    render() {
        return (
            <div>
                {this.renderSearchForm()}
                {this.renderResults()}
            </div>
        );
    }
}

export default SearchPeopleView;