import React, { Component } from 'react';
import NavigationView from '../views/NavigationView';
import HomeView from '../views/HomeView';
import SearchPeopleView from '../views/SearchPeopleView';
import EditPersonView from '../views/EditPersonView';
import TreeView from '../views/TreeView';

class ApplicationController extends Component {
    constructor() {
        super();

        this.state = {
            displayTab: 'home'
        };

        this.headerLinkClicked = this.headerLinkClicked.bind(this);
        this.renderContents = this.renderContents.bind(this);
        this.onEditPerson = this.onEditPerson.bind(this);
    }

    headerLinkClicked(link) {
        this.setState({displayTab: link});
    }

    renderHome() {
        return <HomeView />;
    }

    renderSearchPeople() {
        return (<SearchPeopleView onEditPerson={this.onEditPerson} />);
    }

    renderTree() {
        return (<TreeView />);
    }

    renderPerson() {
        return (<EditPersonView identifier={this.state.selectedPerson} />);
    }

    renderContents() {
        switch(this.state.displayTab) {
            case 'home': return this.renderHome();
            case 'searchPeople': return this.renderSearchPeople();
            case 'tree': return this.renderTree();
            case 'person': return this.renderPerson();
        }
    }

    onEditPerson(identifier) {
        this.setState({displayTab: 'person', selectedPerson: identifier});
    }

    render() {
        return (
            <div className='container-fluid' >
                <NavigationView linkClicked={this.headerLinkClicked} selected={this.state.displayTab} />
                <main style={{paddingTop:'10px'}}>{this.renderContents()}</main>
            </div>
        );
    }
}

export default ApplicationController;