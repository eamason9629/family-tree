package com.divisiblebyzero.familytree.tree.dto

class TreeNodeDTO {
    UUID id
    String name
    Date birthDate
    Date deathDate
    TreeNodeDTO mother
    TreeNodeDTO father
}
