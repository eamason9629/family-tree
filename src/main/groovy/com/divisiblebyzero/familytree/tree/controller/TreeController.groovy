package com.divisiblebyzero.familytree.tree.controller

import com.divisiblebyzero.familytree.tree.dto.TreeNodeDTO
import com.divisiblebyzero.familytree.tree.service.TreeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import static org.springframework.web.bind.annotation.RequestMethod.GET

@RestController
class TreeController {
    @Autowired
    TreeService treeService

    @RequestMapping(value = '/tree/{id}', method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    TreeNodeDTO generateFromId(@PathVariable('id') UUID id) {
        treeService.generateFromRootPersonId(id)
    }
}
