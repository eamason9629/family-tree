package com.divisiblebyzero.familytree.tree.service

import com.divisiblebyzero.familytree.person.entity.Person
import com.divisiblebyzero.familytree.person.repository.PersonRepository
import com.divisiblebyzero.familytree.tree.dto.TreeNodeDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TreeService {
    @Autowired
    PersonRepository personRepository

    TreeNodeDTO generateFromRootPerson(Person rootPerson, int depth = 0) {
        TreeNodeDTO result = new TreeNodeDTO(id: rootPerson.id, birthDate: rootPerson.birthDate, deathDate: rootPerson.deathDate, name: rootPerson.toString())
        if(depth >= 5) {
            return result
        }
        if(rootPerson.father) {
            TreeNodeDTO father = generateFromRootPerson(rootPerson.father, depth + 1)
            result.father = father
        }
        if(rootPerson.mother) {
            TreeNodeDTO mother = generateFromRootPerson(rootPerson.mother, depth + 1)
            result.mother = mother
        }
        return result
    }

    TreeNodeDTO generateFromRootPersonId(UUID id) {
        Person rootPerson = personRepository.findById(id).get()
        return generateFromRootPerson(rootPerson)
    }
}
