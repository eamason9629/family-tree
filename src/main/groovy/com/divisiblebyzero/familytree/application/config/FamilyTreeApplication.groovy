package com.divisiblebyzero.familytree.application.config

import groovy.util.logging.Slf4j
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Slf4j
@SpringBootApplication
@EntityScan(basePackages = ['com.divisiblebyzero.**.entity'])
@ComponentScan(basePackages = ['com.divisiblebyzero.**.bootstrap', 'com.divisiblebyzero.**.service', 'com.divisiblebyzero.**.controller'])
@EnableJpaRepositories(basePackages = ['com.divisiblebyzero.**.repository'])
class FamilyTreeApplication {
	static void main(String[] args) {
		SpringApplication.run(FamilyTreeApplication, args)
	}

	@Bean
	CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		log.trace('Current beans...')
		ctx.getBeanDefinitionNames().sort().each { String beanName ->
			log.trace(beanName)
		}
		return null
	}
}
