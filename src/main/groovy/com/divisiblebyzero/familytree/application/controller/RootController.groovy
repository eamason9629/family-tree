package com.divisiblebyzero.familytree.application.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class RootController {
    @RequestMapping('/')
    ModelAndView index() {
        return new ModelAndView(model: [:], viewName: 'index')
    }
}
