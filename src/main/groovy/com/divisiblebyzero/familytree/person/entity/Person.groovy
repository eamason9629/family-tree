package com.divisiblebyzero.familytree.person.entity

import com.divisiblebyzero.familytree.person.enums.Gender
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.annotations.JoinColumnOrFormula
import org.hibernate.annotations.JoinColumnsOrFormulas
import org.hibernate.annotations.JoinFormula

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Transient

@Entity
class Person {
    @Id
    UUID id = UUID.randomUUID()
    String firstName
    String middleName
    String lastName
    String nickname
    String maidenName
    Gender gender
    Date birthDate
    String placeOfBirth
    Date deathDate
    String placeOfDeath
    String findAGraveLink
    String notes

    @ManyToOne
    Person mother

    @ManyToOne
    Person father

    @Transient
    @JoinColumnsOrFormulas([
            @JoinColumnOrFormula(formula = @JoinFormula('select p from Person where p.mother.id = id or p.father.id = id'))
    ])
    List<Person> children = []

    @JsonProperty
    String getIdentifier() {
        return id.toString()
    }

    @Override
    String toString() {
        String result = firstName
        if(middleName) {
            result += " $middleName"
        }
        if(nickname) {
            result += " '$nickname'"
        }
        if(maidenName) {
            result += " ($maidenName)"
        }
        result += " $lastName"
        return result
    }
}
