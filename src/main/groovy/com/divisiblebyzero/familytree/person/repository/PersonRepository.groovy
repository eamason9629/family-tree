package com.divisiblebyzero.familytree.person.repository

import com.divisiblebyzero.familytree.person.entity.Person
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

@RepositoryRestResource(collectionResourceRel = 'people', path = 'people')
interface PersonRepository extends PagingAndSortingRepository<Person, UUID> {
    List<Person> findByFirstName(@Param('firstName') String firstName)
    List<Person> findByLastName(@Param('lastName') String lastName)

    @RestResource(path = 'people', rel = 'people')
    List<Person> findByFirstNameLikeOrMiddleNameLikeOrLastNameLikeOrNicknameLikeOrMaidenNameLikeOrPlaceOfBirthLikeOrPlaceOfDeathLikeAllIgnoreCase(@Param('firstName') String firstName, @Param('middleName') String middleName, @Param('lastName') String lastName, @Param('nickname') String nickname, @Param('maidenName') String maidenName, @Param('placeOfBirth') String placeOfBirth, @Param('placeOfDeath') String placeOfDeath)
}
