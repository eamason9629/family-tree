package com.divisiblebyzero.familytree.person.bootstrap

import com.divisiblebyzero.familytree.person.entity.Person
import com.divisiblebyzero.familytree.person.repository.PersonRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

import javax.transaction.Transactional

import static com.divisiblebyzero.familytree.person.enums.Gender.female
import static com.divisiblebyzero.familytree.person.enums.Gender.male

@Slf4j
@Component
class PersonDataInitializer implements CommandLineRunner {
    @Autowired
    PersonRepository personRepository

    @Override
    @Transactional
    void run(String... args) throws Exception {
        Map<String, Person> people = [
                erik: new Person(firstName: 'Erik', middleName: 'Anthony', lastName: 'Mason', nickname: 'Pear', gender: male),
                janice: new Person(firstName: 'Janice', middleName: 'Jaye', lastName: 'Mason', nickname: 'Pan', maidenName: 'Hilliard', gender: female),
                kate: new Person(firstName: 'Kathryn', middleName: 'Jaye', lastName: 'Mason', nickname: 'Kate', gender: female),
                ben: new Person(firstName: 'Benjamin', middleName: 'David', lastName: 'Mason', nickname: 'Ben', gender: male),
                dave: new Person(firstName: 'David', middleName: 'Eldon', lastName: 'Mason', nickname: 'Dave', gender: male),
                nancy: new Person(firstName: 'Nancy', middleName: 'Jean', lastName: 'Mason', maidenName: 'Hammond', gender: female),
                gil: new Person(firstName: 'Gilbert', middleName: 'Eldon', lastName: 'Mason', nickname: 'Gil', gender: male),
                fran: new Person(firstName: 'Francis', lastName: 'Mason', nickname: 'Fran', maidenName: 'Anderson', gender: female),
                gilbert: new Person(firstName: 'Gilbert', middleName: 'Eldon', lastName: 'Mason', gender: male),
                mildred: new Person(firstName: 'Mildred', lastName: 'Mason', maidenName: 'Meyer', gender: female),
                charles: new Person(firstName: 'Charles', middleName: 'F', lastName: 'Meyer', nickname: 'Charley', gender: male),
                emma: new Person(firstName: 'Emma', middleName: 'Mae', lastName: 'Meyer', maidenName: 'Gibson', gender: female),
                clarence: new Person(firstName: 'Clarence', middleName: 'E', lastName: 'Gibson', gender: male),
                kateGibson: new Person(firstName: 'Kate', lastName: 'Gibson', gender: female),
                john: new Person(firstName: 'John', lastName: 'Gibson', gender: male),
                hester: new Person(firstName: 'Hester', middleName: 'Ann', lastName: 'Gibson', maidenName: 'Lathrop', gender: female),
                allen: new Person(firstName: 'Allen', middleName: 'Smith', lastName: 'Lathrop', gender: male),
                azubah: new Person(firstName: 'Azubah', lastName: 'Lathrop', maidenName: 'Ackley', gender: female),
                azariah: new Person(firstName: 'Azariah', middleName: 'Allen', lastName: 'Lathrop', gender: male),
                lucy: new Person(firstName: 'Lucy', middleName: 'L', lastName: 'Mallory', gender: female)
        ]
        log.trace("Creating ${people.size()} people!")
        personRepository.saveAll(people.values())

        people.kate.mother = people.janice
        people.kate.father = people.erik
        people.ben.mother = people.janice
        people.ben.father = people.erik
        people.erik.mother = people.nancy
        people.erik.father = people.dave
        people.dave.mother = people.fran
        people.dave.father = people.gil
        people.gil.mother = people.mildred
        people.gil.father = people.gilbert
        people.mildred.mother = people.emma
        people.mildred.father = people.charles
        people.emma.mother = people.kateGibson
        people.emma.father = people.clarence
        people.kateGibson.mother = people.hester
        people.kateGibson.father = people.john
        people.hester.mother = people.azubah
        people.hester.father = people.allen
        people.allen.mother = people.lucy
        people.allen.father = people.azariah

        personRepository.saveAll(people.values())
    }
}
