package com.divisiblebyzero.familytree.person.enums

enum Gender {
    male, female, unknown, undeclared
}
