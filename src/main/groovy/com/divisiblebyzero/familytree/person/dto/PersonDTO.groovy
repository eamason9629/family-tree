package com.divisiblebyzero.familytree.person.dto

class PersonDTO {
    UUID id
    String firstName
    String middleName
    String lastName
    String maidenName
}
