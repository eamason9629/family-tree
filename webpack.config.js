module.exports = {
    entry: './src/main/resources/static/driver.js',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }
        ]
    },
    resolve: {
        extensions: ['.js']
    },
    output: {
        path: __dirname + '/build/resources/main/static',
        publicPath: '/',
        filename: 'bundle.js'
    }
};